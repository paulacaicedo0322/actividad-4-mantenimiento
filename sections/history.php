<?php
    $titulo = '¿Historia del Mantenimiento de software?';
    $imagen = 'images/historia.png';
    $contenido = '<div class="row">
                    <div class="col-12">
                        <p>
                            El mantenimiento de software ha evolucionado desde los primeros sistemas informáticos 
                            en la década de 1960, centrado en correcciones y cambios mínimos. En los años 70 y 80, 
                            se adoptaron metodologías estructuradas y se enfocó en la prevención y adaptación. En 
                            las décadas siguientes, surgieron enfoques ágiles, integración continua y reingeniería, 
                            respondiendo a la creciente complejidad de los sistemas. Hoy, el mantenimiento de software 
                            se enfoca en la sostenibilidad a largo plazo, con énfasis en modularidad, calidad y adaptabilidad 
                            a las últimas tecnologías.
                        </p>
                    </div>
                    <div class="col-12">
                        <hr>    
                            <h3 style="text-align: center;">Acontecimientos importantes</h3>
                        <hr>    
                    </div>
                    <div class="col-4">
                        <h4>Modelo de Waterfall (1970):</h4>
                        <p>
                            La introducción del modelo de desarrollo en cascada marcó un hito importante en el 
                            mantenimiento de software. Este enfoque estructurado dividió el ciclo de vida del desarrollo 
                            de software en fases secuenciales, facilitando la identificación y corrección de errores 
                            durante la fase de mantenimiento.
                        </p> 
                    </div>
                    <div class="col-4">
                    <h4>ISO/IEC 12207 (1995):</h4>
                        <p>
                            La publicación de la norma ISO/IEC 12207 estableció estándares para los procesos de ciclo 
                            de vida del software, incluido el mantenimiento. Proporcionó un marco reconocido internacionalmente
                            que guió las mejores prácticas en el desarrollo y mantenimiento de software, contribuyendo a la 
                            mejora de la calidad y eficiencia.
                        </p> 
                    </div>
                    <div class="col-4">
                        <h4>Movimiento hacia Metodologías Ágiles (2001 en adelante):</h4>
                        <p>
                            surgimiento del Manifiesto Ágil y la adopción de metodologías ágiles, como Scrum y Kanban, 
                            revolucionaron el mantenimiento de software. Estas metodologías priorizan la adaptabilidad, 
                            la colaboración y la entrega continua, permitiendo respuestas rápidas a los cambios en los 
                            requisitos y mejorando la eficiencia en el mantenimiento a lo largo del tiempo.
                        </p> 
                    </div>
                </div>
                </div>';
    include('sections_template.php');
?>
