<?php
$titulo = 'Progresos significativos del Mantenimiento de software';
$imagen = ''; 
$contenido = ' <div class="row">
                    <div class="col-6">
                        <div class="row">
                            <div class="col-12">
                                <h4>Integración Continua y Despliegue Continuo (CI/CD):</h4>
                            </div>
                            <div class="col-8">
                                <p>
                                    La adopción generalizada de prácticas de Integración Continua (CI) y Despliegue Continuo (CD) ha sido un avance significativo. 
                                    Estas prácticas automatizan las pruebas y la entrega de software, facilitando la detección temprana de errores y permitiendo 
                                    actualizaciones más rápidas y seguras en los sistemas en producción.
                                </p> 
                            </div>
                            <div class="col-4">
                                <img src="images/integracion.png" alt="" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="row">
                            <div class="col-12">
                                <h4>Enfoque Ágil y DevOps:</h4>
                            </div>
                            <div class="col-8">
                                <p>
                                    La transición hacia enfoques ágiles y la incorporación de prácticas de DevOps han mejorado la colaboración entre equipos 
                                    de desarrollo y operaciones. Esto ha llevado a ciclos de desarrollo más rápidos, mejorando la capacidad de respuesta a 
                                    cambios y optimizando la eficiencia en el mantenimiento del software a lo largo del tiempo. La cultura DevOps promueve 
                                    la automatización, la colaboración y la entrega continua para lograr un desarrollo y mantenimiento más efectivos.
                                </p> 
                            </div>
                            <div class="col-4">
                                <img src="images/deploy.png" alt="" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>';
include('sections_template.php');
?>
