<?php
    $titulo     = '¿Qué es el Mantenimiento de software?';
    $imagen     = 'images/mantenimiento.png';
    $contenido  = 'El mantenimiento de software se refiere al conjunto de actividades 
                    realizadas después del desarrollo inicial de un programa informático .
                    para garantizar su buen funcionamiento, adaptarlo a los cambios en el entorno, 
                    corregir errores, mejorar su rendimiento y añadir nuevas características. 
                    Estas tareas son esenciales para garantizar la calidad y la eficacia a lo largo 
                    del ciclo de vida del software, ya que permiten que la aplicación evolucione para 
                    satisfacer las necesidades cambiantes de los usuarios y del entorno tecnológico. 
                    Las actividades de mantenimiento 
                    se dividen comúnmente en cuatro categorías: correctivo, preventivo, adaptativo y perfectivo.';
    include('sections_template.php');
?>
