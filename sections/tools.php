<?php
$titulo = 'Herramientas importante en el Mantenimiento de software';
$imagen = ''; 
$contenido = '<div class="row">
                    <div class="col-4">
                        <h3>Control de Versiones: Git:</h3>
                        <p>
                            Git es un sistema de control de versiones distribuido ampliamente utilizado.
                            Permite realizar un seguimiento de los cambios en el código fuente a lo largo del tiempo, facilitando la 
                            colaboración entre desarrolladores y ayudando a revertir o fusionar cambios de manera eficiente.
                        </p>
                    </div>
                    <div class="col-4">
                        <h3>Sistemas de Seguimiento de Problemas: Jira:</h3>
                        <p>
                            ira es una herramienta de gestión de proyectos y seguimiento de problemas que 
                            se utiliza ampliamente en el desarrollo de software. Permite a los equipos registrar 
                            y dar seguimiento a errores, tareas y mejoras en el software a lo largo del ciclo de vida del proyecto.
                        </p>
                    </div>
                    <div class="col-4">
                        <h3>Herramientas de Pruebas Automatizadas: Selenium:</h3>
                        <p>
                            Selenium es una suite de herramientas para la automatización de pruebas en aplicaciones web. 
                            Permite la grabación y reproducción de acciones del usuario en un navegador, facilitando la 
                            creación y ejecución de pruebas automatizadas.
                        </p>
                    </div>
                </div>';
include('sections_template.php');
?>