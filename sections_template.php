<section>
    <div>
        <h4><?= $titulo ?></h4>
        <!-- Estructura común para la imagen y el contenido -->
        <div class="row">
            <div class="col-6">
                <img src="<?= $imagen ?>" alt="" class="form-control">
            </div>
            <div class="col-6">
                <p><?= $contenido ?></p>
            </div>
        </div>
    </div>
</section>